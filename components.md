At a high level, Austin contains functionality for the following components:

* Client management
* Designer management
    * Typical CRUD actions
    * Availabilities
    * Invoicing
* Project management
    * Typical CRUD actions
    * Project progress follow-up
    * Client communication follow-up
* AI-driven designer matching
* Message management
    * Typical CRUD actions
    * Overview of all incoming and outgoing communication (designers, clients, administrators)
    * Sending custom messages
* Return brief management
    * Project-specific return briefs
    * Return brief template management
    * Client communication follow-up
* SMS communication (to clients, designers and administrators)