Doctrine Entities
----

All Object-Relational information is captured in Doctrine Entities. They can be found in /src/. [Coding approach](http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/tutorials/getting-started.html#starting-with-the-product) and [database synchronisation](http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/tutorials/getting-started.html#generating-the-database-schema) follow the Doctrine suggestions.

Authentication
----

User authentication is all handled within Austin. Pages can be secured by checking $_SESSION variables. Most Austin pages and API documents contain a header similar to:

````php
<?php

@require_once "bootstrap.php";
@session_start();

$me = null;

if (isset($_SESSION['stylesmith']) && isset($_SESSION['login_hash'])) {
  $_designer = $_SESSION['stylesmith'];
  $_designer = $entityManager->find("Designer", $_designer);

  if ($_designer != null) {

    if ($_designer->getLoginHash() != $_SESSION['login_hash']) {
      session_unset();
      header('Location: /login');
      exit();
    } else {
      // page will only be accessible by Designers
      $me = $_designer;
    }

  } else {
    session_unset();
    header('Location: /login');
    exit();
  }

} else {
  session_unset();
  header('Location: /login');
  exit();
}

````

To allow Admin only access to pages (e.g. project administration, client administration, designer administration), you add the following check:

````php
<?php

// ...

$me = $_designer;

if ($admin_only && !$me->isAdministrator()) {
    header('Location: /index');
    exit();
}

````

Emails and notifications
----

Many of the actions on Austin are communicated to HQ via Email and Push Notifications. These managed in the EmailTemplate repository, which takes care for tempting, sending, database storage and broadcasting a WebRTC Push Notification to listeners. The content of EmailTemplates can be managed by Stylesmiths HQ via Austin (/config/email/), which is retrieved from the database (table emailtemplates).

New notifications require setup in the Notification repository:

````php
<?php

    switch ($this->message) {
        case 'client.project.create': // Notification identifier
            $this->setStyle("danger"); // notification style (visual, either 'danger', 'success', 'warning', 'info')
            $this->link = "/projects/" . $this->project->getToken() . "/details/"; // link within Austin
            $this->relevance = "project"; // Entity that notification primarily relates to
            $this->action = true; // Requires action?
            $this->action_text = "Review details"; // Text for Action button
            return "<span class=\"text-bold\">" . $this->client->getFullName() . "</span> created a new project. Now is the time to review the details and match the project with a designer."; // Text for Notification
            break;

````

The Notification repository will further take care of broadcasting the message.