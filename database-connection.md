Austin relies on a MySQL database. Connection is configured in /bootstrap.php. Here, you need to provide the following details:

```php
<?php

$conn = array(
        'dbname'      => 'austin_database_name',
        'user'        => 'austin_username',
        'password'    => 'austin_password',
        'host'        => 'localhost',
        'port'        => 3306,
        'driver'      => 'pdo_mysql',
        'unix_socket' => 'location_of_mysql_socket' // May not be required with your setup
);

```