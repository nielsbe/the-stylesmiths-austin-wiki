All code is versioned in a private [Gitlab](https://www.gitlab.com) repository, including PHP source files and objects, HTML documents, images, stylesheets, JavaScript documents and API documents. Folders with static components, such as profile pictures and project-specific files, are not versioned. For local versions to work properly, one should create the following folders within the root folder of the project:

* /cdn/
* /cdn/announcements/
* /cdn/billing/
* /cdn/designers/
* /cdn/projects/
* /cdn/prospects/
* /cdn/reports/
* /cdn/spaces/
* /cdn/styles/

All Composer packages (i.e. Doctrine, Stringy) are versioned in the code repository, to allow for new code forks to be launched immediately with a correct and working version of the necessary packages.

Synchronisation
----

Changes can be pushed to the production environment manually (SFTP), but can be automated (sped up) as well via RSYNC. Make sure not to synchronise the /cdn/ folder and /bootstrap.php (because connection information of the development environment is likely different from the production environment).