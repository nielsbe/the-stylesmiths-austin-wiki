Recently, we introduced a new return brief procedure, which introduced radical changes to our existing way of working. Instead of uploading files and requiring admins to review files, designers now have the possibility to create return briefs via a line-by-line entry of tasks. 

All key information regarding new return briefs are captured in the database:

* payment_stages contains a static list of ten payment stages that are supported;
* returnbrief__stages contains a static list of the chronological stages that a return brief passes, from Draft to Accepted by Client; and
* service_categories contains a static list of service categories.

Other relevant tables contain the returnbrief__ prefix.

![Flowchart](img/flowchart_returnbrief.png)