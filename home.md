Austin (external name for The Stylesmiths CRM) relies on a LAMP server infrastructure (Linux, Apache, MySQL, PHP). Our codebase relies on:

* [Doctrine](http://www.doctrine-project.org), an open source package for database storage and mapping;
* [Stringy](https://github.com/danielstjules/Stringy), an open source package for string manipulation;
* [PHPExcel](https://github.com/PHPOffice/PHPExcel), to generate Excel and CSV files from data;
* [PHPMailer](https://github.com/PHPMailer/PHPMailer), an email sending library for PHP;
* [Twilio](https://github.com/twilio/twilio-php), to send SMS messages via PHP; and
* [Pusher](https://github.com/pusher/pusher-http-php), to send realtime messages to clients.

Locally, Doctrine and Stringy are managed via [Composer](https://getcomposer.org). All other libraries are not managed via Composer, because they need less frequent updating.

This WIKI introduces new developers to the setup and approaches:

* [Primary changes](primary-changes.md) outlining key changes to perform when taking over the main development responsibility;
* [Database connection](database-connection.md) with connection and setup information for the Austin MySQL database;
* [Codebase](codebase.md) with key insights into the existing codebase;
* [Coding approach](coding-approach.md) with insights into the coding approach best followed for future development;
* [Components](components.md) containing a high-level overview of the main functional elements of Austin; and
* [Return brief](return-brief.md) with details on the new return brief procedure (which was launched shortly before the full technical handover).